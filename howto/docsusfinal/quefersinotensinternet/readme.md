# QUE FER SI NO TENS INTERNET

![](./PortadaGuifiamunt.png)

### Exemple d’una instal•lació convencional i denominacions dels elements.

![](./Esquema_xarxa.png)

**-POE:**  sigles de Power over Ethernet, Alimentació a través de Ethernet.
 https://es.wikipedia.org/wiki/Power_over_Ethernet

**-ROUTER – PUNT D’ACCÉS:**  és l’element que connecta el nostre ordinador amb l’antena. La connexió amb l’ordinador pot ser per cable o per WiFi.


# QUÈ CAL FER ?

### 1. Comprovar la connexió elèctrica

Primer cal comprovar que tots els elements estiguin connectats i alimentats amb corrent elèctric.

Es recomanable desconnectar el cable ethernet que alimenta l’antena i tornar a connectar-lo al cap d’un minut.


## 2. Comprovar connexió a la xarxa

A continuació, es necessari comprovar que els cables de xarxa estiguin ben connectats.

No podrem comprovar físicament la connexió de l’antena, però podrem fer-ho per programari.


## 3. Accés a l'antena 

Si tots els elements estan correctament connectats, el que ens caldrà és saber si podem accedir fins a l’antena.

Per fer-ho, obrirem una finestra de sistema i escriurem ping i l’adreça IP de l’antena.


# Com podem trobar la nostra adreça IP?

El camí més fàcil per saber l'adreça IP és obrir la pàgina web del nostre node seguint els següents passos:

### Pas 1.-  Anar a un navegador web i escriure l’adreça:  https://guifi.net/ca/barcelona.

Ens apareixerà la següent pàgina web:

![](./WEB_NODE1.PNG)


### Pas 2.- Cercar la nostra zona (per exemple Horta, El Carmel, Montbau, La Teixonera i La Clota).  s’obra la llista dels nodes de la zona

![](./WEB_NODE2.PNG)

### Pas 3.- A continuació apareixeran tots els nodes de la zona. Caldrà seleccionar el nom de nostre node (per exemple JPinos):

![](./LLISTA_NODES1.PNG)
![](./PJPINOS.PNG)

### Pas 4.- S’obrirà la pàgina amb tota la informació relativa al vostre node i l'adreça IP  de l'antena. Com es veu a continuació

![](./DADESNODE.PNG)

Ara que ja sabem l’adreça (en l'exemple 10.139.95.105), caldrà fer un ping per saber si l'antena respon.


# Com podem fer un ping ?

### Pas 1. - Obrir una finestra de sistema.

### Pas 2. - Anar al menú de Windows, cercar Sistema de Windows i escollir Símbolo del sistema.  

![](./SISTEMA.PNG)

Quan cliquem sobre Símbolo del sistema s'obrirà aquesta finestra:

![](./CMD.PNG)

### Pas 3.- Cal escriure ping (espai) i l’adreça de la vostra antena i prémer intro, tal i com es veu continuació

![](./PING.PNG)

Si obtenim una resposta com la que es mostra a la imatge és perquè tenim connexió fins a l’antena i tota la nostra instal•lació és correcta.


# 4.  Comprovar que la nostra antena té connexió fins el node següent.

Com fer-ho ?

### Pas 1. Primer ens cal saber l’adreça IP de l’antena d’aquest altre node.  Per tant, caldrà  tornar a la pàgina web on hem trobat la informació relativa al nostre node. 

I aquesta és l’adreça de l’antena a la que us connecteu;

![](./ADRECAANTENA.PNG)

### Pas 2. Cal repetir el procés de fer ping a aquesta antena per tal de comprovar si tenim connexió amb el supernode al que estem connectats:

![](./PING2.PNG)

Si obtenim una resposta com aquesta, es que tenim connexió fins el supernode següent:

![](./PING3.PNG)

# 5 – Comprovar la VPN (Virtual Private Netword) (Xarxa Privada Virtual)

La VPN es la que ens connecta directament al supernode que ens dóna la sortida a Internet i ens permet navegar per Internet, que està configurada al vostre node.

Com fer-ho ?

### Pas 1.- Obrir una finestra de sistema i fer un ping a l’adreça del servidor que gestiona la vostra connexió a la VPN, en aquest cas la 10.228.199.193.

![](./PINGVPN.PNG)

Si la resposta es aquesta es que teniu connexió.


# 6 – Comprovar si falla el DNS (Domain Name Server)

El DNS es el servei que tradueix els noms que posem a Internet per la seva adreça IP i ens permet accedir-hi.

### Pas 1.- Obrir una finestra de sistema i fer un ping a la adreça d’un servidors de DNS. 8.8.8.8
            Obrir una finestra de sistema i fer un ping a un nom d’Internet, cern.ch
             
![](./PINGDNS.PNG)           

Si la resposta es aquesta, tot està correcta.

Si fetes totes aquestes proves no es connecta, caldrà que us poseu en contacte amb l’administrador del supernode, per tal d'informar-lo del problema que tenim.

# RESUM

Aquestes són unes recomanacions mínimes per poder resoldre la manca de sortida a Internet.

Encara que el més fàcil es que, desprès d’instal•lar i connectar el vostre node, anoteu les següents dades: 

![](./RESUM.PNG)  

# SI TENIU AQUESTA INFORMACIÓ, NO CALDRÀ QUE FEU TOTES LES PASES DESCRITES ANTERIORMENT, SOLS LES DE COMPROVACIÓ :

### 1 – COMPROVAR LES CONNEXIÓS A LA XARXA ELECTRICA
### 2 – LA CONNEXIÓ DE XARXA ETHERNET DE TOTS ELS ELEMENTS.
### 3 – FER PING A LA VOSTRA ANTENA
### 4 – COMPROVAR LA CONNEXIÓ DE LA VPN
### 5 – FER PING AL SUPERNODE.
### 6 – COMPROVAR SI FALLA EL DNS (Domain Name Server)


# S.A.X.

