When you have MTU problems, you cannot navigate to some places. This is a list to test sites

In openwrt you can test MTU problems remotely with `wget --no-check-certificate https://example.com`

# MTU not working

https://gitlab.com
https://www.ubnt.com
https://marca.com
https://easyjet.com
https://elmundo.es

# MTU working

https://google.com
