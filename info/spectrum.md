Interessant, de fet la gracia dels 60 GHz és que no arriba massa lluny i per tant evita rebre interferència de més enllà (per que l'oxigen absorbeix el senyal) i es pot reutilitzar la freqüència a llocs relativament propers (una mena de xarxa cel·lular natural). [Aquesta figura sobre absorció ho deixa clar](http://cfa.aquila.infn.it/wiki.eg-climet.org/images/4/40/Opacity.png)

[Aquí es veu l'abast per un enllaç de 8 Mbps, i com 60 GHz té un abast reduït i per tant es pot reutilitzar una mica més lluny](https://www.rfglobalnet.com/doc/fixed-wireless-communications-at-60ghz-unique-0001)

He vist productes a 24GHz que segur arriben més lluny, també sensibles a la pluja (per tant com diu l'Oriol convé backup a 5GHz) com [air fiber 24 GHz](https://www.ubnt.com/airfiber/airfiber24/)

però no se si cal llicència, per aquesta ref sembla que no: https://en.wikipedia.org/wiki/ISM_band

src https://llistes.guifi.net/sympa/arc/guifi-users/2018-09/msg00022.html
